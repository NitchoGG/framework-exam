module.exports = (mongoose) => {
    let express = require('express');
    let router = express.Router();

    let categorySchema = mongoose.Schema({
        id: Number,
        name: String,
        description : String
    });

    let categoryModel = mongoose.model("categories", categorySchema);

    router.get('/', (req, res) => {
        categoryModel.find({}).sort({"name":"ascending"}).exec((err,categories) =>{
            if(err) return console.log(err);
            res.json(categories);
        })
    });

    router.get('/:id', (req, res) => {
        categoryModel.findOne({id:req.params.id},(err,category) => {
            if(err) return console.log(err);
            res.json(category);
        })
    });

    return router;
};