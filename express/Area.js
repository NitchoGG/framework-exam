module.exports = (mongoose) => {
    let express = require('express');
    let router = express.Router();

    let areaSchema = mongoose.Schema({
        id: Number,
        name: String
    });

    let areaModel = mongoose.model("area", areaSchema);

    router.get('/', (req, res) => {
        areaModel.find({}).sort({"name":"ascending"}).exec((err,categories) =>{
            if(err) return console.log(err);
            res.json(categories);
        })
    });

    router.get('/:id', (req, res) => {
        areaModel.findOne({id:req.params.id},(err,category) => {
            if(err) return console.log(err);
            res.json(category);
        })
    });

    return router;
};