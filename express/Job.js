module.exports = (mongoose) => {
    let express = require('express');
    let router = express.Router();

    let jobSchema = mongoose.Schema({
        title: String,
        description : String,
        area : String,
        category : String
    });

    let jobModel = mongoose.model("job", jobSchema);

    router.get('/:id', (req, res) => {
        jobModel.findOne({_id:req.params.id}).exec((err,job) =>{
            if(err) return console.log(err);
            res.json(job);
        })
    });

    router.post('/getJobs', (req, res) => {
        jobModel.find({category: {$regex: new RegExp(req.body.category, 'i')},area: {$regex: new RegExp(req.body.area, 'i')}}).exec((err,category) => {
            if(err) return console.log(err);
            res.json(category);
        })
    });

    router.post('/createOffer', (req, res) => {
        let tempJob = new jobModel({
            title : req.body.title,
            description : req.body.description,
            area : req.body.area,
            category : req.body.category
        });

        tempJob.save((err) =>{
            if(err) {
                console.log(err);
            }
            else{
                res.json({msg: "Job offer Created"});
            }
        });
    });

    return router;
};