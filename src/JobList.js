import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import JobListItem from "./JobListItem"

export default class CategoryList extends Component {

    constructor(props) {
        super(props);
        //let category = encodeURIComponent(this.props.category).replace(/%20/g, '-');
        //let area = encodeURIComponent(this.props.area).replace(/%20/g, '-');
        this.props.getJobs(this.props.category,this.props.area);
    }

    render() {
        let jobs= "";
        let list = [];
        this.props.jobs.forEach((elm) => {
            list.push(<JobListItem key={elm._id} job = {elm} />);
        });

        if(list.length > 0){

            jobs =
                <div className="container">
                    <div className={"row headerList"}>
                        <h2>Jobs i {this.props.area} som {this.props.category}</h2>
                    </div>
                    {list}
                </div>
            ;
        }
        else{
            jobs =
                <div className="container">
                    <div className={"row headerList"}>
                        <h2>Jobs i {this.props.area} som {this.props.category}</h2>
                    </div>
                    <p>Der er ingen job opslag i det given område. Gå tilbage til start siden og prøve og søg på noget andet</p>
                    <button className={"btn btn-primary"} onClick={() => this.props.history.push("/")}>Tilbage</button>
                </div>
        }

        return jobs;
    }
}