import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import CategoryListItem from "./CategoryListItem"

export default class CategoryList extends Component {

    constructor(props) {
        super(props);
        this.props.getCategories();
    }

    render() {
        let list = [];

        this.props.categories.forEach((elm) => {
            list.push(<CategoryListItem key={elm._id} category = {elm} />);
        });

        return (
            <div className="container">
                <div className={"row headerList"}>
                    <h2>Job kategorier</h2>
                </div>
                <div className={"testContainer"}>
                {list}
                </div>
            </div>
        );
    }
}