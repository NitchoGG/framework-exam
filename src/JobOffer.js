import React, {Component} from 'react';
import { Link } from 'react-router-dom';

export default class JobOffer extends Component {

    constructor(props) {
        super(props);

        this.props.getJob(this.props.id);
    }

    render() {
        return (
            <div className="container">
                <div className={"row jobOffer"}>
                    <h2>{this.props.selectedJob.title}</h2>
                    <p>{this.props.selectedJob.description}</p>
                    <p>Din arbejdsplads ligger omkring {this.props.selectedJob.area}</p>
                    <p>Du kommer til at arbejde med {this.props.selectedJob.category}</p>
                    <button className={"btn btn-primary"}>Ansøg her</button>
                </div>
            </div>
        );
    }
}