import React, {Component} from 'react';
import { Link } from 'react-router-dom';

export default class Profile extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        if (!this.props.auth.loggedIn()) this.props.history.push('/');
    }

    logout = (e) => {
        e.preventDefault();
        this.props.logout();
        this.props.history.goBack();
    };

    render() {
        return (
            <div className="container">
                <div className={"profile"}>
                    <Link to={`/create-job`}>
                        Create Job Offer
                    </Link>
                    <Link to={`/change-password`}>
                        Change Password
                    </Link>
                    <button onClick={this.logout} className={"btn btn-primary"}>Logout</button>
                </div>
            </div>
        );
    }
}