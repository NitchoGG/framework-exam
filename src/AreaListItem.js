import React, {Component} from 'react';
import {Link} from 'react-router-dom';

export default class AreaListItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        let url = this.props.area.name;
        return (
            <div className="row category">
                <div className={"categoryContent"}>
                    <Link to={`/jobs/${this.props.category}/${url}`} className={"categoryLink"}>
                        <h3>{this.props.area.name}</h3>
                    </Link>
                </div>
            </div>
        );
    }
}