import React, {Component} from 'react';
import {BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom';

import './App.css';
import AuthService from './AuthService'
import Navbar from './Navbar'
import Login from './Login'
import CategoryList from './CategoryList'
import AreaList from './AreaList'
import JobList from './JobList'
import JobOffer from './JobOffer'
import Profile from './Profile'
import ChangePassword from './ChangePassword'
import CreateJobOffer from './CreateJobOffer'

class App extends Component {
    API_URL = process.env.REACT_APP_API_URL;

    constructor(props) {
        super(props);

        this.Auth = new AuthService(`${this.API_URL}/user/authenticate`);
        this.state = {
            categories : [],
            areas : [],
            jobs : [],
            specificJob : "",
            user : {
                username : "Guest"
            }
        };
    }

    getCategories = () =>{
        this.Auth.fetch(`${this.API_URL}/category`)
            .then(categories => {
                this.setState({
                    categories: categories
                });
            })
            .catch(error => {
                // TODO: Inform the user about the error
                console.error("Error when fetching: ", error);
            })
    };

    getAreas = () =>{
        this.Auth.fetch(`${this.API_URL}/area`)
            .then(areas => {
                this.setState({
                    areas: areas
                });
            })
            .catch(error => {
                // TODO: Inform the user about the error
                console.error("Error when fetching: ", error);
            })
    };

    getJob = (id) =>{
        this.Auth.fetch(`${this.API_URL}/job/${id}`)
            .then(job => {
                this.setState({
                    specificJob: job
                });
            })
            .catch(error => {
                // TODO: Inform the user about the error
                console.error("Error when fetching: ", error);
            })
    };

    getJobs = (category,area) =>{
        let json = {
            category:category,
            area:area
        };

        this.Auth.fetch(`${this.API_URL}/job/getJobs`, {
            method: 'POST',
            body: JSON.stringify(json),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
            }).then(jobs => {
                this.setState({
                    jobs: jobs
                });
            })
            .catch(error => {
                // TODO: Inform the user about the error
                console.error("Error when fetching: ", error);
            })
    };

    createUser = (json) => {
        return fetch(`${this.API_URL}/user/create`, {
            method: 'POST',
            body: JSON.stringify(json),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        }).then(res => res.json())
            .then(user => {
                if(user.msg === undefined){
                    this.setState({ user: user })
                }
                return Promise.resolve(user);
            })
    };

    getUser = (json) => {
        fetch(`${this.API_URL}/user/authenticate`, {
            method: 'POST',
            body: JSON.stringify(json),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        }).then(res => res.json())
            .then(user => {
                if(user.msg === undefined){
                    this.setState({ user: user })
                }
                return Promise.resolve(user);
            })
    };

    createJob = (json) => {
        return this.Auth.fetch(`${this.API_URL}/job/createOffer`, {
            method: 'POST',
            body: JSON.stringify(json),
        }).then((res) => {
            return Promise.resolve(res)
        })
    };

    componentWillMount() {
        this.changeLoginStatus();
    }

    changePassword = (json) => {
        return this.Auth.fetch(`${this.API_URL}/user/`,{
            method: "PUT",
            body: JSON.stringify(json)
        }).then((res) => {
            return Promise.resolve(res)
        })
    };

    changeLoginStatus = () => {
        let userinfo = this.Auth.getUserInfo();
        this.setState({user: userinfo ? userinfo:this.state.user})
    };

    logout = () => {
        this.Auth.logout();
        this.setState({user: {
                username: "Guest"
            }});
    };


    render() {
        return (
            <Router>
                <div>
                    <Navbar user = {this.state.user} auth = {this.Auth}/>
                    <div className="container-fluid contentSpacing">
                        <Switch>
                            <Route exact path="/" render={(props) => <CategoryList key={"categoryList"} {...props} categories={this.state.categories} getCategories={this.getCategories} />}/>
                            <Route exact path="/jobs/:category" render={(props) => <AreaList key={"areaList"} {...props} areas={this.state.areas} getAreas={this.getAreas} category = {props.match.params.category}/>}/>
                            <Route exact path="/jobs/:category/:area" render={(props) => <JobList key={"jobList"} {...props} jobs={this.state.jobs} getJobs={this.getJobs} category = {props.match.params.category} area = {props.match.params.area}/>}/>
                            <Route exact path="/show-job/:id" render={(props) => <JobOffer key={"jobOffer"} {...props} id = {props.match.params.id} getJob={this.getJob} selectedJob = {this.state.specificJob}/>}/>
                            <Route exact path="/profile" render={(props) => <Profile key={"profile"} {...props} auth = {this.Auth} user = {this.state.user} logout = {this.logout}/>}/>
                            <Route exact path="/change-password" render={(props) => <ChangePassword key={"profile"} {...props} auth = {this.Auth} changePassword={this.changePassword}/>}/>
                            <Route exact path="/create-job" render={(props) => <CreateJobOffer key={"profile"} {...props} auth = {this.Auth} categories={this.state.categories} getCategories={this.getCategories} areas={this.state.areas} getAreas={this.getAreas} createJob = {this.createJob}/>}/>
                            <Route exact path="/login" render={(props) => <Login {...props} auth={this.Auth} getUser={this.getUser} createUser={this.createUser} changeLoginStatus={this.changeLoginStatus} />} />
                        </Switch>
                    </div>
                </div>
            </Router>
        );
    }
}

export default App;
