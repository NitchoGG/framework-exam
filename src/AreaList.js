import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import AreaListItem from "./AreaListItem"

export default class AreaList extends Component {

    constructor(props) {
        super(props);
        this.props.getAreas();
    }

    render() {
        let list = [];

        this.props.areas.forEach((elm) => {
            list.push(<AreaListItem key={elm._id} area = {elm} category = {this.props.category} />);
        });

        return (
            <div className="container">
                <div className={"row headerList"}>
                    <h2>Job områder</h2>
                </div>
                <div className={"testContainer"}>
                {list}
                </div>
            </div>
        );
    }
}