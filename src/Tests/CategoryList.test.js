import React from "react"
import {BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom';
import { render } from "@testing-library/react"
import CategoryListItem from "../CategoryListItem";
import CategoryList from "../CategoryList";

it('renders CategoryListItem and checks if our value is Handel og service', () => {
    const {getByText} = render(<Router><CategoryListItem key={testCategories[0].key.value} category = {testCategories[0]} /></Router>);
    expect(getByText("Handel og service")).toBeInTheDocument();
});

it('renders CategoryList with 4 elements and check if there is 4', () => {
    const {getComponent} = render(<Router><CategoryList key={testCategories} categories={testCategories} getCategories={jest.fn()} /></Router>);
    expect(document.querySelector(".testContainer").childNodes.length).toBe(4);
});