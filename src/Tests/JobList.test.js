import React from "react"
import {BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom';
import { render } from "@testing-library/react"
import JobList from "../JobList";

it('renders JobList without any jobs and then checks if the text is there', () => {
    const {getByText} = render(<Router><JobList key={"jobList"} jobs={[]} getJobs={jest.fn()} category = {testJobs[0].category} area = {testJobs[0].area}/></Router>);
    expect(getByText("Der er ingen job opslag i det given område. Gå tilbage til start siden og prøve og søg på noget andet")).toBeInTheDocument();
});

it('renders JobList with a job and check there is 2 child nodes in category content. The title and description', () => {
    const {getByText} = render(<Router><JobList key={"jobList"} jobs={testJobs} getJobs={jest.fn()} category = {testJobs[0].category} area = {testJobs[0].area}/></Router>);
    expect(document.querySelector(".categoryContent").childNodes.length).toBe(2);
});