import React from "react"
import {BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom';
import { render } from "@testing-library/react"
import AreaListItem from "../AreaListItem";
import AreaList from "../AreaList";
import CategoryList from "../CategoryList";

it('renders App category list item with the name of Bornholm', () => {
    const {getByText} = render(<Router><AreaListItem key={testAreas[0].key.value} area = {testAreas[0]} category = {testAreas[0].category.value} /></Router>);
    expect(getByText("Bornholm")).toBeInTheDocument();
});

it('renders AreaList with 4 elements', () => {
    const {getComponent} = render(<Router><AreaList areas={testAreas} getAreas={jest.fn()} category = {testAreas[0].category.value}/>></Router>);
    expect(document.querySelector(".testContainer").childNodes.length).toBe(4);
});

