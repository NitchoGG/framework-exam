import React, {Component} from 'react';
import {Link} from 'react-router-dom';

export default class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            signUp: false
        };
    }

    spanClick = () => {
        this.setState({
            signUp: true
        });
    };

    handleLogin = (e) => {
        e.preventDefault();
        this.props.auth.login(this.username.value.toLowerCase(), this.password.value)
            .then(response => {
                if (response.msg === 'User authenticated successfully') {
                    this.props.changeLoginStatus();
                    this.props.history.push(`/`)
                } else if (response.msg === 'Username or password missing!') {
                    this.lbl.innerHTML = "Username or password missing!";
                } else if (response.msg === "Password mismatch!") {
                    this.lbl.innerHTML = "Wrong password!";
                } else {
                    this.lbl.innerHTML = "Wrong username";
                }
            })
    };

    handleRegister = (e) => {
        e.preventDefault();
        if(this.password.value === this.password2.value)
        {
            this.props.createUser({
                username: this.username.value.toLowerCase(),
                password: this.password.value,
            }).then(response => {
                 if(response.msg === "User created") {
                     this.lbl.innerHTML = "User created, now being redirected!";
                     setTimeout(() => {
                         this.props.history.push(`/`)
                     }, 3000);
                }
                 else{
                     this.lbl.innerHTML = response.msg;
                 }
            });
        }
        else{
            this.lbl.innerHTML = "Passwords do not match";
        }
    };

    render() {
        let content =
            <div className={"container loginContainer"}>
                <form className={"loginForm"}>
                    <label className="err" ref={lbl => this.lbl = lbl}/>
                    <input ref={username => this.username = username} type="text" placeholder={"Username"}/>
                    <input type="password" ref={password => this.password = password} placeholder={"Password"}/>
                    <button type={"submit"} className={"btn btn-primary"} onClick={this.handleLogin}>Sign in</button>
                </form>
                <p>Don't have an account? <span onClick={this.spanClick} className={"signUp"}>Sign up</span></p>
            </div>;

        if (this.state.signUp) {
            content =
                <div className={"container loginContainer"}>
                    <form className={"loginForm"}>
                        <label className="err" ref={lbl => this.lbl = lbl}/>
                        <input ref={username => this.username = username} type="text" placeholder={"Username"}/>
                        <input type="password" ref={password => this.password = password} placeholder={"Password"}/>
                        <input type="password" ref={password2 => this.password2 = password2} placeholder={"Retype password"}/>
                        <button type={"submit"} className={"btn btn-primary"} onClick={this.handleRegister}>Create
                            Account
                        </button>
                    </form>
                </div>;
        }

        return content;
    }
}