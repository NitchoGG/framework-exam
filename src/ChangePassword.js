import React, {Component} from 'react';

export default class ChangePassword extends Component {

    constructor(props) {
        super(props);
    }

    componentWillMount() {
        if (!this.props.auth.loggedIn()) this.props.history.push('/');
    }

    onChangePasswordClick = (e) => {
        e.preventDefault();
        if (this.newPassword1.value !== this.newPassword2.value){
            this.lbl.innerHTML = "Passwords dont match!!";
        }
        else
        {
            this.props.changePassword({password: this.oldPassword.value, newpassword: this.newPassword1.value}).then(response => {
                if (response.msg === "Password Updated") {
                    this.lbl.innerHTML = response.msg + " - now being redirected to your profile page";
                    setTimeout(() => {
                        this.props.history.push(`/profile`)
                    }, 3000);
                }
                else{
                    this.lbl.innerHTML = response.msg
                }
            })
        }
    };

    render() {
        return (
            <div className="container loginContainer">
                <form className={"loginForm"}>
                    <label className="err" ref={lbl => this.lbl = lbl} />
                    <input ref={oldPassword => this.oldPassword = oldPassword} type="text" className="text-cos" placeholder="Old Password" />
                    <input ref={newPassword1 => this.newPassword1 = newPassword1} type="text" className="text-cos" placeholder="New Password" />
                    <input ref={newPassword2 => this.newPassword2 = newPassword2} type="text" className="text-cos" placeholder="Repeat Password" />
                    <button onClick={this.onChangePasswordClick} className="btn-primary btn">Change Password</button>
                </form>
            </div>
        );
    }
}