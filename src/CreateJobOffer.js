import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import { DropdownList } from 'react-widgets'
import 'react-widgets/dist/css/react-widgets.css';
import CategoryListItem from "./CategoryListItem";

export default class CreateJobOffer extends Component {

    constructor(props) {
        super(props);
        this.props.getCategories();
        this.props.getAreas();

        this.state = {
            category: 'Handel og service',
            area:"Bornholm"
        }
    }

    componentWillMount() {
        if (!this.props.auth.loggedIn()) this.props.history.push('/');
    }

    createJob = (e) => {
        e.preventDefault();
        this.props.createJob({title : this.jobTitle.value, description : this.jobDescription.value, area : this.state.area, category : this.state.category}).then(response => {
            this.lbl.innerHTML = response.msg;
        });
    };

    render() {
        let categoriesName =[];
        let areaNames = [];

        this.props.categories.forEach((elm) => {
            categoriesName.push(elm.name);
        });

        this.props.areas.forEach((elm) => {
            areaNames.push(elm.name);
        });
        return (
            <div className="container jobOfferContainer">
                <p>Job title</p>
                <input ref={jobTitle => this.jobTitle = jobTitle} className={"jobOfferInput"} id="inputTitle" placeholder={"Skriv en titel til dit jobopslag"} />

                <p>Job Beskrivelse</p>
                <textarea ref={jobDescription => this.jobDescription = jobDescription} className={"jobOffertxtArea"} id="inputBody" placeholder={"Skriv en beskrivelse af dit job opslag"}/>

                <p>Vælg en kategori</p>
                <DropdownList
                    data={categoriesName}
                    value={this.state.category}
                    onChange={value => this.setState({ category:value })}
                />

                <p>Vælg et område</p>
                <DropdownList
                    data={areaNames}
                    value={this.state.area}
                    onChange={value => this.setState({ area:value })}
                />

                <button onClick={this.createJob} className={"btn btn-primary jobOfferBTN"}>Opret Job</button>
                <label className="err" ref={lbl => this.lbl = lbl} />
            </div>
        );
    }
}