import React, {Component} from 'react';
import {Link} from 'react-router-dom';

export default class JobListItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="row category">
                <Link to={`/show-job/${this.props.job._id}`}>
                    <div className={"categoryContent"}>
                        <h2>{this.props.job.title}</h2>
                        <p>{this.props.job.description}</p>
                    </div>
                </Link>
            </div>
        );
    }
}