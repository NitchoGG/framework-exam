import React, {Component} from 'react';
import { Link } from 'react-router-dom';

export default class Navbar extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let login ="";

        if(this.props.auth.loggedIn()) {
            login =
                <div className={"ml-auto mr-1"}>
                <label className={"userText"}>Logged in as {this.props.user.username}</label>
                <Link to={"/profile"} className={"btn btn-primary "}> Account </Link>
            </div>;
        }
        else{
            login =
                <div className={"ml-auto mr-1"}>
                    <label className={"userText"}>{this.props.user.username} account</label>
                    <Link to={'/login'} className={"btn btn-primary "}> Login </Link>
                </div>;
        }

        return (
            <nav className="navbar navbar-light bg-light fixedNav">
                <div className="container-fluid">
                    <Link to={'/'} className="navbar-brand"> Home </Link>
                    {login}
                </div>
            </nav>
        );
    }
}

