import '@testing-library/react/cleanup-after-each'
import "jest-dom/extend-expect"

global.testCategories = [
    {
        name : "Handel og service",
        description : "Der er jobs indenfor : Rengøring, restauration, detail",
        key : "1",
        listKey : "11"
    },
    {
        name : "Industri og håndværk",
        description : "Der er jobs indenfor : Byggeri, landbrug, transport",
        key : "2",
        listKey : "12"
    },
    {
        name : "Informationsteknologi",
        description : "Der er jobs indenfor : Systemudvikling, it-support osv.",
        key : "3",
        listKey : "13"
    },
    {
        name : "Ingeniør og teknik",
        description : "Der er jobs indenfor : Elektronik, medicinal, kemi",
        key : "4",
        listKey : "14"
    }];

global.testAreas = [
    {
        name : "Bornholm",
        category : "Handel og Service",
        key : "1"
    },
    {
        name : "Fyn",
        category : "Handel og Service",
        key : "2"
    },
    {
        name : "Nordsjælland",
        category : "Handel og Service",
        key : "3"
    },
    {
        name : "Region Midtjylland",
        category : "Handel og Service",
        key : "4"
    },
];

global.testJobs =[
    {
        title: "Systemudvikler",
        description : "Har du lyst til at arbejde med salg af og rådgivning indenfor Collabor...",
        area :  "Bornholm",
        category :  "Informationsteknologi"
    }
];