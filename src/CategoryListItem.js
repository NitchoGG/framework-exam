import React, {Component} from 'react';
import {Link} from 'react-router-dom';

export default class CategoryListItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        let url = this.props.category.name;
        return (
            <div className="row category">
                <div className={"categoryContent"}>
                    <Link to={`/jobs/${url}`} className={"categoryLink"}>
                        <h3>{this.props.category.name}</h3>
                    </Link>
                    <p>Der er jobs indenfor : {this.props.category.description}</p>
                </div>
            </div>
        );
    }
}